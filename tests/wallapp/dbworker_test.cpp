#include "wall.hpp"
#include "gtest/gtest.h"
#include <memory>
#include <string>
#include <utility>
#include <vector>

const std::string db_name = "test_db";
const std::string table_name = "test_table";

class DBWorkerTest : public testing::Test {
protected:
  void SetUp() override {
    using std::literals::operator""s;

    if (test_case.empty()) {
      for (int i = 1; i <= 30; i++) {
        test_case.push_back({"test_name#"s + std::to_string(i),
                             "test message #"s + std::to_string(i)});
      }
    }

    try {
      pqxx::nontransaction w(base_connection);
      w.exec("CREATE DATABASE "s + db_name);

      db_connection = std::make_unique<pqxx::connection>("dbname="s + db_name);
      pqxx::nontransaction w2(*db_connection);
      w2.exec0(
          "CREATE TABLE "s + table_name +
          " (id SERIAL PRIMARY KEY, name varchar(32), message varchar(5000), "
          "date timestamp DEFAULT CURRENT_TIMESTAMP)");

      for (auto &t : test_case) {
        w2.exec0("INSERT INTO "s + table_name + " VALUES(DEFAULT, \'" +
                 t.first + "\', \'" + t.second + "\', DEFAULT)");
      }

    } catch (...) {
      FAIL() << "something wrong with database. Plsease check if postgresql is "
                "working and test databases aren't created";
    }
  }

  void TearDown() override {
    using std::literals::operator""s;

    db_connection.reset();

    pqxx::nontransaction w(base_connection);
    w.exec0("DROP DATABASE "s + db_name);
  }

  pqxx::connection base_connection{"dbname=postgres"};
  std::unique_ptr<pqxx::connection> db_connection{nullptr};
  std::vector<std::pair<std::string, std::string>> test_case;
};

TEST_F(DBWorkerTest, creation_test) {
  using namespace server;
  using std::literals::operator""s;

  TearDown();

  db::DBWorker worker(db_name, table_name);

  pqxx::work w(base_connection);
  auto res =
      w.exec1("SELECT EXISTS (SELECT datname FROM pg_catalog.pg_database "
              "WHERE datname = \'"s +
              db_name + "\')"s);
  w.commit();

  EXPECT_EQ(res.at(0).view(), "t");

  db_connection = std::make_unique<pqxx::connection>("dbname="s + db_name);
  pqxx::work w2(*db_connection);
  auto res2 = w2.exec1(
      "SELECT EXISTS (SELECT table_name FROM information_schema.tables "
      "WHERE table_name = \'"s +
      table_name + "\')"s);
  w2.commit();

  EXPECT_EQ(res2.at(0).view(), "t");
}

TEST_F(DBWorkerTest, add_message_test) {
  using namespace server;
  using std::literals::operator""s;

  db::DBWorker worker(db_name, table_name);

  const std::string test_name = "testname";
  const std::string test_message = "this is a test message";

  worker.addMessage(test_name, test_message);

  pqxx::work w(*this->db_connection);
  auto res(w.exec1("SELECT T.message FROM "s + table_name +
                   " AS T WHERE T.name = \'"s + test_name + "\'"));

  EXPECT_EQ(res.at(0).view(), test_message);
}

TEST_F(DBWorkerTest, delete_message_test) {
  using namespace server;
  using std::literals::operator""s;

  db::DBWorker worker(db_name, table_name);

  worker.deleteMessage("2");

  pqxx::work w(*this->db_connection);

  auto res(w.exec1("SELECT EXISTS (SELECT * FROM "s + table_name +
                   " WHERE name = \'" + test_case[1 /*id = 2*/].first + "\')"));

  EXPECT_EQ(res.at(0).view(), "f");
}
TEST_F(DBWorkerTest, get_messages_test) {
  using namespace server;
  using std::literals::operator""s;

  db::DBWorker worker(db_name, table_name);

  auto result = worker.getMessages(10, 10);

  EXPECT_EQ(result.size(), 10);
  int i = 10;
  for (auto &r : result) {
    EXPECT_EQ(r.name, test_case[i++].first);
  }
}
