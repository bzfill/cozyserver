#include "applist.hpp"
#include "config.hpp"
#include "http.hpp"
#include "wall.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <gmock/gmock-spec-builders.h>
#include <memory>
#include <string>

using namespace server;

class DBWorkerMock : public db::DBWorkerInterface {
public:
  DBWorkerMock(){};
  virtual ~DBWorkerMock(){};

  MOCK_METHOD(db::MessagesType, getMessages, (size_t, size_t), (override));
  MOCK_METHOD(void, addMessage, (std::string, std::string), (override));
  MOCK_METHOD(void, deleteMessage, (std::string), (override));
};

class WallAppTest : public testing::Test {
protected:
  void SetUp() {
    app = std::make_unique<WallApp>();
    app->setDBInterface(db);

    //AppTableType applist = {makeAppTableElement<WallApp>()};
    // server::configuration::server_config.initFromApps(applist);
  }

  void TearDown() {
    app->defaultConfig();
    app.reset();
    // server::configuration::server_config.drop();
  }

  std::unique_ptr<WallApp> app;
  std::shared_ptr<DBWorkerMock> db = std::make_shared<DBWorkerMock>();
};

TEST_F(WallAppTest, get_messages) {
  using namespace testing;
  using std::literals::operator""s;

  std::map<std::string, std::string> variables;
  variables["limit"] = "3";
  variables["offset"] = "5";

  db::MessagesType test_messages;
  for (int i = 0; i < 3; i++) {
    db::MessageType m;

    m.id = std::to_string(i);
    m.name = "test_name#"s + std::to_string(i);
    m.message = "test message #"s + std::to_string(i);
    m.date = "2020-06-22 17:"s + std::to_string(i) + ":30.795424";

    test_messages.push_back(m);
  }

  EXPECT_CALL(*db.get(), getMessages(5, 3)).WillOnce(Return(test_messages));

  json expected_result;

  for (auto &m : test_messages) {
    expected_result["messages"] += {{"id", m.id},
                                    {"name", m.name},
                                    {"message", m.message},
                                    {"date", m.date}};
  }

  auto result = app->executeMethod("get_messages", variables);

  EXPECT_EQ(result.dump(), expected_result.dump());
}

TEST_F(WallAppTest, add_messages) {
  using namespace testing;
  using std::literals::operator""s;

  std::map<std::string, std::string> variables;
  variables["name"] = "TEST_NAME";
  variables["message"] = "TEST_MESSAGE";

  EXPECT_CALL(*db.get(), addMessage("TEST_NAME", "TEST_MESSAGE"))
      .WillOnce(Return());

  json expected_result;

  expected_result["STATUS"] = "OK";

  auto result = app->executeMethod("add_message", variables);

  EXPECT_EQ(result.dump(), expected_result.dump());
}

TEST_F(WallAppTest, form) {
  using namespace testing;
  using std::literals::operator""s;

  std::string test_content =
      "-----------------------------9051914041544843365972754266\r\n";
  test_content += "Content-Disposition: form-data; name=\"name\"\r\n\r\n";
  test_content += "TEST_NAME\r\n";
  test_content +=
      "-----------------------------9051914041544843365972754266\r\n";
  test_content += "Content-Disposition: form-data; name=\"message\"\r\n\r\n";
  test_content += "TEST_MESSAGE\r\n";
  test_content +=
      "-----------------------------9051914041544843365972754266--\r\n";

  http::Request req;
  req.method = http::Method::POST;
  req.path = "/";
  req.version = http::Version::HTTP_1_1;
  req.content = test_content;
  req.content_type = http::ContentType::multipart_form_data;
  req.boundary = "---------------------------9051914041544843365972754266";
  EXPECT_CALL(*db.get(), addMessage("TEST_NAME", "TEST_MESSAGE"))
      .WillOnce(Return());

  app->run(req);
}
