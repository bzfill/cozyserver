#include "config.hpp"
#include "http.hpp"
#include "static_content.hpp"
#include "storeapp.hpp"
#include "gtest/gtest.h"
#include <memory>

struct appResult {
  std::string data;
  size_t chunk_count;
};

class StorageTest : public testing::Test {
protected:
  void SetUp() override {
    // server::settings::STORAGE_ROOT = ".";
    // server::settings::STORAGE_CHUNK_SIZE = chunk_size;
    auto &config = server::configuration::ConfigManager::getInstance();
    app = std::make_unique<server::Storage>();
    config.set(app->name(), "path", std::string("."));
    config.set(app->name(), "chunk_size", chunk_size);
    createTestFile();
  }

  void TearDown() override {
    app->defaultConfig();
    app.reset();
    removeTestFile();
  }

  std::string testData;
  const std::string testFileName = "sample.html";

  void createTestFile() {
    const std::string sample_16 = "abcdefghijklmnop";

    testData = "";

    // 1 MiB
    for (int i = 0; i < 64; i++) {
      testData += sample_16;
    }

    std::ofstream os(testFileName);
    os << testData;
  }

  void removeTestFile() {
    if (std::filesystem::exists(testFileName)) {
      std::filesystem::remove(testFileName);
    }
  }

  appResult runApp(server::http::Request req) {
    appResult ret;
    std::string result;

    auto tr = app->run(req);
    result += tr.data;

    int i = 1;
    while (!tr.finish) {
      tr = app->next();
      result += tr.data;
      i++;
    }

    ret.chunk_count = i;
    ret.data = result;

    return ret;
  }

  std::unique_ptr<server::Storage> app;
  int chunk_size = 128;
};

class loadFileTester : public server::Storage {
public:
  loadFileTester(){this->setRoot(".");} TaskResult
      runLoadFile(std::string path) {
    return Storage::loadFile(path);
  }
  TaskResult next() { return Storage::next(); }
};

TEST_F(StorageTest, basic) {
  using namespace server;
  using std::literals::operator""s;

  http::Request req;

  req.method = http::Method::GET;
  req.path = "/"s + testFileName;
  req.version = http::Version::HTTP_1_1;

  auto result = runApp(req);

  EXPECT_EQ(result.chunk_count, testData.length() / chunk_size);

  auto body = result.data.substr(result.data.find("\r\n\r\n") + 4,
                                 result.data.length());

  EXPECT_EQ(testData, body);
}

TEST_F(StorageTest, loadFile) {
  using namespace server;
  using std::literals::operator""s;

  auto path = "/"s + testFileName;
  loadFileTester tester;
  auto tr = tester.runLoadFile(path);

  std::string result = tr.data;

  int i = 1;
  while (!tr.finish) {
    tr = tester.next();
    result += tr.data;
    i++;
  }

  EXPECT_EQ(i, testData.length() / chunk_size);

  auto body = result.substr(result.find("\r\n\r\n") + 4, result.length());

  EXPECT_EQ(testData, body);
}

TEST_F(StorageTest, CheckNotFoundError) {
  using namespace server;

  http::Request req;

  req.method = http::Method::GET;
  req.path = "/wrongpath.txt";
  req.version = http::Version::HTTP_1_1;

  auto result = runApp(req);

  EXPECT_EQ(static_content::RESPONSE_404_NOT_FOUND.gen(), result.data);
}

TEST_F(StorageTest, CheckRelativePathFilter) {
  using namespace server;
  using std::literals::operator""s;

  http::Request req;

  req.method = http::Method::GET;
  req.path = "/../storeapp/"s + testFileName;
  req.version = http::Version::HTTP_1_1;

  auto result = runApp(req);

  EXPECT_EQ(static_content::RESPONSE_404_NOT_FOUND.gen(), result.data);
}
