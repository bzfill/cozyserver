#include "application.hpp"
#include "config.hpp"
#include "json.hpp"
#include "gtest/gtest.h"
#include <filesystem>
#include <fstream>
#include <memory>

using namespace server::configuration;

constexpr int test_int_val = 42;
constexpr char test_int_name[] = "prop_int";

constexpr char test_str_val[] = "test string";
constexpr char test_str_name[] = "prop_str";

constexpr char test_filename[] = "test_config.json";

class TestApp : public server::Application {
public:
  virtual std::string name() const override final { return "test"; }
  virtual std::string pathPrefix() const override final { return "test"; }
  virtual std::string description() const override final { return "test"; }
  // return a function which creates an object of this class
  virtual CreateAppFuncType sharedPtrLambda() override final {
    return []() -> std::shared_ptr<server::Application> {
      return std::make_shared<TestApp>();
    };
  }
  virtual void defaultConfig() override {
    config.prop_int.setDefault();
    config.prop_str.setDefault();
  }
  virtual TaskResult run(server::http::Request) override {
    TaskResult tr;
    return tr;
  };
  virtual TaskResult next() override {
    TaskResult tr;
    return tr;
  };
  static struct Config {
    Property<TestApp, int> prop_int{test_int_name, test_int_val};
    Property<TestApp, std::string> prop_str{test_str_name, test_str_val};
  } config;
};

TestApp::Config TestApp::config;

class ConfigTest : public testing::Test {
protected:
  void SetUp() override { app.defaultConfig(); }
  void TearDown() override {
    if (std::filesystem::exists(test_filename)) {
      std::filesystem::remove(test_filename);
    }
  }

  void createTestFile(int int_val, std::string str_val) {
    nlohmann::json j;
    j[app.name()][test_int_name] = int_val;
    j[app.name()][test_str_name] = str_val;

    std::ofstream os(test_filename);
    os << j.dump();
  }

  bool checkFile(int int_val, std::string str_val) {
    nlohmann::json j;
    std::ifstream is(test_filename);
    j = nlohmann::json::parse(is);

    try {
      if (j.at(app.name()).at(test_int_name) == int_val &&
          j.at(app.name()).at(test_str_name) == str_val) {
        return true;
      } else {
        return false;
      }
    } catch (...) {
      return false;
    }
  }

  ConfigManager &config = ConfigManager::getInstance();
  TestApp app;
};

TEST_F(ConfigTest, default_values) {
  EXPECT_EQ(std::any_cast<int>(config.get(app.name(), test_int_name)),
            test_int_val);
  EXPECT_EQ(std::any_cast<std::string>(config.get(app.name(), test_str_name)),
            test_str_val);
}

TEST_F(ConfigTest, set) {
  app.config.prop_int.set(100);
  app.config.prop_str.set("new value");

  EXPECT_EQ(std::any_cast<int>(config.get(app.name(), test_int_name)), 100);
  EXPECT_EQ(std::any_cast<std::string>(config.get(app.name(), test_str_name)),
            std::string("new value"));
}

TEST_F(ConfigTest, get) {
  EXPECT_EQ(app.config.prop_int.get(), test_int_val);
  EXPECT_EQ(app.config.prop_str.get(), test_str_val);

  app.config.prop_int.set(100);
  app.config.prop_str.set("new value");

  EXPECT_EQ(app.config.prop_int.get(), 100);
  EXPECT_EQ(app.config.prop_str.get(), "new value");
}

TEST_F(ConfigTest, exists) {
  EXPECT_EQ(config.exists(app.name(), test_int_name), true);
  EXPECT_EQ(config.exists(app.name(), test_str_name), true);
  EXPECT_EQ(config.exists(app.name(), "wrong_prop"), false);
  EXPECT_EQ(config.exists("wrong_app", test_str_name), false);
}

TEST_F(ConfigTest, file_read) {
  constexpr int test_file_int = 99;
  constexpr char test_file_str[] = "different test value";
  createTestFile(test_file_int, test_file_str);
  config.loadFromFile(test_filename);

  EXPECT_EQ(app.config.prop_int.get(), test_file_int);
  EXPECT_EQ(app.config.prop_str.get(), std::string(test_file_str));
}

TEST_F(ConfigTest, file_write) {
  constexpr int test_file_int = 200;
  constexpr char test_file_str[] = "another test value";

  app.config.prop_int.set(test_file_int);
  app.config.prop_str.set(test_file_str);

  config.createFile(test_filename);
  EXPECT_TRUE(checkFile(test_file_int, test_file_str));
}
