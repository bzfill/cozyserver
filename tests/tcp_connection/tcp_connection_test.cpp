#include "application.hpp"
#include "server.hpp"
#include "static_content.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <gmock/gmock-actions.h>
#include <gmock/gmock-cardinalities.h>
#include <gmock/gmock-matchers.h>
#include <gmock/gmock-spec-builders.h>
#include <memory>
#include <sstream>

#define ASYNC_WRITE_ACTION                                                     \
  Invoke(                                                                      \
      [](boost::asio::ip::tcp::socket &socket,                                 \
         const boost::asio::mutable_buffers_1 &buf,                            \
         std::function<void(const boost::system::error_code &, std::size_t)>   \
             lambda) {                                                         \
        boost::system::error_code ec{};                                        \
        lambda(ec, buf.size());                                                \
      })

#define ASYNC_READ_UNTIL_ACTION(DATA)                                          \
  Invoke(                                                                      \
      [](boost::asio::ip::tcp::socket &socket, boost::asio::streambuf &buf,    \
         std::string delim,                                                    \
         std::function<void(const boost::system::error_code &, std::size_t)>   \
             lambda) {                                                         \
        std::string str = DATA;                                                \
        std::ostream os(&buf);                                                 \
        os << str;                                                             \
        boost::system::error_code ec{};                                        \
        lambda(ec, str.length());                                              \
      })

using namespace server;

class FakeApp : public Application {
public:
  MOCK_METHOD(std::string, name, (), (const override));
  MOCK_METHOD(std::string, pathPrefix, (), (const override));
  MOCK_METHOD(std::string, description, (), (const override));
  MOCK_METHOD(TaskResult, run, (http::Request), (override));
  MOCK_METHOD(TaskResult, next, (), (override));
  MOCK_METHOD(std::function<std::shared_ptr<Application>()>, sharedPtrLambda,
              (), (override));
};

class TcpSocketWrapperFake : public TcpSocketWrapperInterface {
public:
  MOCK_METHOD(boost::asio::io_context &, getContext, (), (override));

  MOCK_METHOD(
      void, async_read_until,
      (boost::asio::ip::tcp::socket & socket, boost::asio::streambuf &buf,
       std::string delim,
       std::function<void(const boost::system::error_code &, std::size_t)>
           lambda),
      (override));

  MOCK_METHOD(
      void, async_write,
      (boost::asio::ip::tcp::socket & socket,
       const boost::asio::mutable_buffers_1 &buf,
       std::function<void(const boost::system::error_code &, std::size_t)>
           lambda),
      (override));

  MOCK_METHOD(
      void, async_read_exactly,
      (boost::asio::ip::tcp::socket & socket, boost::asio::streambuf &buf,
       std::size_t len,
       std::function<void(const boost::system::error_code &, std::size_t)>
           lambda),
      (override));
};

const std::string br = "\r\n\r\n";
MATCHER_P(BufEq, buf, "") {
  std::string str(boost::asio::buffers_begin(arg),
                  boost::asio::buffers_end(arg));

  return str == buf;
}

std::shared_ptr<FakeApp> app;
boost::asio::io_context dummy_context;

class TcpConnectionTest : public testing::Test {
protected:
  void SetUp() override {
    using namespace testing;
    app = std::make_shared<FakeApp>();
    ON_CALL(wrapper, getContext()).WillByDefault(ReturnRef(dummy_context));
    ON_CALL(*app, sharedPtrLambda())
        .WillByDefault(
            Return([]() -> std::shared_ptr<Application> { return app; }));
    ON_CALL(*app, name()).WillByDefault(Return("fakeapp"));
  }

  void TearDown() override {
    app.reset();
    tcp_connection.reset();
  }
  std::shared_ptr<TcpConnection> tcp_connection;
  TcpSocketWrapperFake wrapper;
};

TEST_F(TcpConnectionTest, basic) {
  using namespace testing;

  static constexpr char test_request[] = "GET /fakeapp/path HTTP/1.1";

  EXPECT_CALL(wrapper, async_read_until(_, _, br, _))
      .WillOnce(ASYNC_READ_UNTIL_ACTION(test_request))
      .WillOnce(Return());

  std::vector<Application::TaskResult> tr = {
      {false, "FIRST"}, {false, "SECOND"}, {true, "THIRD"}};

  http::Request req(test_request);
  req.path = "/path";
  EXPECT_CALL(*app, run(req)).WillOnce(Return(tr[0]));
  EXPECT_CALL(*app, next).WillOnce(Return(tr[1])).WillOnce(Return(tr[2]));
  for (auto task : tr) {
    EXPECT_CALL(wrapper, async_write(_, BufEq(task.data), _))
        .WillOnce(ASYNC_WRITE_ACTION);
  }

  std::map<std::string, Application::CreateAppFuncType> app_map_test = {
      {app->name(), app->sharedPtrLambda()}};

  tcp_connection = TcpConnection::create(wrapper);
  tcp_connection->setAppTable(app_map_test);
  tcp_connection->start();
}

TEST_F(TcpConnectionTest, checkNotFoundError) {
  using namespace testing;
  static constexpr char test_request[] = "GET /wrong/path HTTP/1.1";

  EXPECT_CALL(wrapper, async_read_until(_, _, br, _))
      .WillOnce(ASYNC_READ_UNTIL_ACTION(test_request))
      .WillOnce(Return());

  EXPECT_CALL(
      wrapper,
      async_write(_, BufEq(static_content::RESPONSE_404_NOT_FOUND.gen()), _));

  std::map<std::string, Application::CreateAppFuncType> app_map_test = {
      {app->name(), app->sharedPtrLambda()}};

  tcp_connection = TcpConnection::create(wrapper);
  tcp_connection->setAppTable(app_map_test);
  tcp_connection->start();
}

TEST_F(TcpConnectionTest, silentApp) {
  using namespace testing;
  static constexpr char test_request[] = "GET /fakeapp/path HTTP/1.1";
  EXPECT_CALL(wrapper, async_read_until(_, _, br, _))
      .WillOnce(ASYNC_READ_UNTIL_ACTION(test_request))
      .WillOnce(Return());

  http::Request req(test_request);
  req.path = "/path";

  Application::TaskResult tr = {false, ""};
  EXPECT_CALL(*app, run(req)).WillOnce(Return(tr));
  EXPECT_CALL(
      wrapper,
      async_write(
          _, BufEq(static_content::RESPONSE_500_INTERNAL_SERVER_ERROR.gen()),
          _))
      .WillOnce(ASYNC_WRITE_ACTION);

  std::map<std::string, Application::CreateAppFuncType> app_map_test = {
      {app->name(), app->sharedPtrLambda()}};

  tcp_connection = TcpConnection::create(wrapper);
  tcp_connection->setAppTable(app_map_test);
  tcp_connection->start();
}
