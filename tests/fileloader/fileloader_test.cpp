#include "fileloader.hpp"
#include "gtest/gtest.h"
#include <_types/_uint8_t.h>
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <memory>

class FilereaderTest : public testing::Test {
protected:
  void SetUp() override { createTestFile(); }

  void TearDown() override {
    reader.reset();
    removeTestFile();
  }

  std::unique_ptr<server::FileReader> reader;
  std::string testData;
  const std::string testFileName = "sample.txt";

  void createTestFile() {
    const std::string sample_16 = "abcdefghijklmnop";

    testData = "";

    // 1 MiB
    for (int i = 0; i < 64; i++) {
      testData += sample_16;
    }

    std::ofstream os(testFileName);
    os << testData;
    os.close();
  }

  void removeTestFile() {
    if (std::filesystem::exists(testFileName)) {
      std::filesystem::remove(testFileName);
    }
  }
};

TEST_F(FilereaderTest, basic) {
  reader = std::make_unique<server::FileReader>(testFileName, 7);
  EXPECT_EQ(reader->getFileSize(), testData.length());

  std::string data;
  while (!reader->noMoreChunks()) {
    auto &&chunk = reader->getChunk();
    data.insert(data.end(), begin(chunk), end(chunk));
  }

  EXPECT_EQ(data.size(), testData.length());

  if (!std::equal(data.begin(), data.end(), begin(testData))) {
    FAIL();
  }
}

TEST_F(FilereaderTest, file_not_found) {
  try {
    reader = std::make_unique<server::FileReader>("wrong.txt");
  } catch (server::FileNotFoundError &e) {
    EXPECT_EQ(std::string(e.what()),
              std::string(server::FileNotFoundError("wrong.txt").what()));
  } catch (...) {
    FAIL();
  }
}
