#include "api.hpp"
#include "application.hpp"
#include "applist.hpp"
#include "http.hpp"
#include "json.hpp"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include <gmock/gmock-spec-builders.h>
#include <memory>

using namespace server;

class FakeApiApp : public Application, public APIInterface {
public:
  MOCK_METHOD(std::string, name, (), (const override));
  MOCK_METHOD(std::string, pathPrefix, (), (const override));
  MOCK_METHOD(std::string, description, (), (const override));
  MOCK_METHOD(TaskResult, run, (http::Request), (override));
  MOCK_METHOD(TaskResult, next, (), (override));
  MOCK_METHOD(std::function<std::shared_ptr<Application>()>, sharedPtrLambda,
              (), (override));
  MOCK_METHOD(json, executeMethod,
              (const std::string &method,
               (const std::map<std::string, std::string> &variables)),
              (const override));
};

static std::shared_ptr<FakeApiApp> fake_app;

class ApiTest : public testing::Test {
public:
  void SetUp() override {
    using testing::Return;
    fake_app = std::make_shared<FakeApiApp>();
    app = std::make_unique<API>();

    ON_CALL(*fake_app, sharedPtrLambda())
        .WillByDefault(
            Return([]() -> std::shared_ptr<Application> { return fake_app; }));
    ON_CALL(*fake_app, name()).WillByDefault(Return("fakeapp"));

    table = {{fake_app->name(), fake_app->sharedPtrLambda()}};
    app->setAppTable(table);
  }

  void TearDown() override {
    app.reset();
    fake_app.reset();
  }

  std::unique_ptr<API> app;
  const std::string test_api_method = "test_method";

private:
  AppTableType table;
};

TEST_F(ApiTest, basic) {
  using std::literals::operator""s;
  using testing::Return;

  http::Request req;
  req.method = http::Method::GET;
  req.path = "/"s + fake_app->name() + "/" + test_api_method;
  req.version = http::Version::HTTP_1_1;
  req.variables = {{"test_var", "value"}, {"another_var", "42"}};

  json test_result;
  test_result["test_field"] = "test_value";
  EXPECT_CALL(*fake_app, executeMethod(test_api_method, req.variables))
      .WillOnce(Return(test_result));

  auto result = app->run(req).data;

  auto body = result.substr(result.find(http::br) + http::br.length(),
                            std::string::npos);
  EXPECT_EQ(body, test_result.dump());
}
