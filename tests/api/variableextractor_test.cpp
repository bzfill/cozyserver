#include "application.hpp"
#include "varextractor.hpp"
#include "gtest/gtest.h"

class VETest : public server::APIInterface {
public:
  virtual json executeMethod(
      const std::string &method,
      const std::map<std::string, std::string> &variables) const override {
    json ret;
    return ret;
  }

  void test() {
    std::map<std::string, std::string> vars = {{"test_int", "20"},
                                               {"test_int_neg", "-32"},
                                               {"test_str", "some_string"}};
    server::VariableExtractor ve{vars};

    // test int

    int res_int = ve.get<int>("test_int");
    EXPECT_EQ(res_int, 20);

    // test negative int

    int res_int_neg = ve.get<int>("test_int_neg");
    EXPECT_EQ(res_int_neg, -32);

    // test string

    std::string res_str = ve.get<std::string>("test_str");
    EXPECT_EQ(res_str, "some_string");

    // test string with default char*
    std::string res_str_def_c = ve.get("test_str", "default");
    EXPECT_EQ(res_str_def_c, "some_string");

    // test string with default string
    std::string res_str_def = ve.get("test_str", std::string("default"));
    EXPECT_EQ(res_str_def, "some_string");

    // test default value
    int res_def = ve.get("wrong", -99);
    EXPECT_EQ(res_def, -99);
  }
};

TEST(VariableExtractrorTest, basic) {
  VETest vet;
  vet.test();
}
