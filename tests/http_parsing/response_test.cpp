#include "http.hpp"
#include "server_definitions.hpp"
#include "gtest/gtest.h"
#include <boost/algorithm/string.hpp>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <vector>

using namespace server::http;
using std::literals::operator""s;

const std::string test_content = "TEST CONTENT MESSAGE";
TEST(response, basic) {

  server::http::Response resp;
  resp.content_type = ContentType::text_html;
  resp.content_encoding = ContentEncoding::identity;
  resp.connection = Connection::CLOSE;
  //resp.transfer_encoding = TransferEncoding::identity;
  resp.status_code = StatusCode::C200_OK;
  resp.content = test_content;

  auto result = resp.gen();

  const std::string br = "\r\n";
  auto status_line = "HTTP/1.1 200 OK"s;
  auto first_line = result.substr(0, result.find(br));
  ASSERT_EQ(status_line, first_line);

  std::vector<std::string> cut_substrings = {
      status_line + br,
      "Content-Type: text/html"s + br,
      "Content-Encoding: identity"s + br,
      "Connection: close"s + br,
      "Access-Control-Allow_origin: *"s + br,
      "X-Frame-Options: DENY"s + br,
      "Server: "s + SERVER_NAME + br,
      "Content-Length: "s + std::to_string(test_content.length()) + br,
      "Date: ",
      " UTC"s + br,
      br,
      test_content,
  };

  // after all removes only date must be in the string
  for (auto &substr : cut_substrings) {
    ASSERT_TRUE(result.find(substr) != std::string::npos) << substr;
    boost::replace_first(result, substr, "");
  }

  std::tm tm;
  std::istringstream ss(result);
  ss >> std::get_time(&tm, SERVER_TIME_FORMAT);
  ASSERT_FALSE(ss.fail()) << result;
}
