#include "http.hpp"
#include "gtest/gtest.h"

const std::string request_line_test = "PUT /test/path HTTP/1.1";
const std::string br = "\r\n";

TEST(request, reqest_line) {
  // std::string req_str = request_line_test;
  server::http::Request req(request_line_test);
  ASSERT_EQ(req.method, server::http::Method::PUT);
  ASSERT_EQ(req.path, "/test/path");
  ASSERT_EQ(req.version, server::http::Version::HTTP_1_1);
}

TEST(request, request_header) {
  std::string req_str = request_line_test;
  req_str += br + "Accept-Language: en-us";
  server::http::Request req(req_str);
  ASSERT_EQ(req.headers["Accept-Language"], "en-us");
}

TEST(request, variable) {
  std::string req_str = "GET /test/path?number=12345 HTTP/1.1";
  server::http::Request req(req_str);
  ASSERT_EQ(req.variables["number"], "12345");
}

TEST(request, variables) {
  std::string req_str =
      "GET /test/path?number=12345&text=hello&date=23.05.2020 HTTP/1.1";
  server::http::Request req(req_str);
  ASSERT_EQ(req.variables["number"], "12345");
  ASSERT_EQ(req.variables["text"], "hello");
  ASSERT_EQ(req.variables["date"], "23.05.2020");
}

TEST(request, variables_with_junk) {
  std::string req_str =
      "GET /test/path?number=12345&&&text=hello&&date=23.05.2020 HTTP/1.1";
  server::http::Request req(req_str);
  ASSERT_EQ(req.variables["number"], "12345");
  ASSERT_EQ(req.variables["text"], "hello");
  ASSERT_EQ(req.variables["date"], "23.05.2020");
}

TEST(request, get_path_at) {
  std::string req_str =
      "GET "
      "/test/path/little/bit/more?number=12345&&&text=hello&&date=23.05.2020 "
      "HTTP/1.1";
  server::http::Request req(req_str);
  std::vector<std::string> path_list = {"test", "path", "little",
                                        "bit",  "more", ""};
  int i = 0;
  for (auto &pl : path_list) {
    ASSERT_EQ(req.getPathAt(i++), pl);
  }
}

TEST(request, get_path_root) {
  std::string req_str = "GET / HTTP/1.1";
  server::http::Request req(req_str);

  ASSERT_EQ(req.getPathAt(0), "");
}

TEST(request, cut) {
  std::string req_str =
      "GET "
      "/test/path/little/bit/more?number=12345&&&text=hello&&date=23.05.2020 "
      "HTTP/1.1";
  server::http::Request req(req_str);

  std::vector<std::string> path_list = {"path", "little", "bit", "more", ""};
  for (auto &pl : path_list) {
    req.cutBeginingOfPath();
    ASSERT_EQ(req.getPathAt(0), pl);
  }
}
