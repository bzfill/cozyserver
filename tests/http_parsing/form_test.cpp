#include "http.hpp"
#include "gtest/gtest.h"

using namespace server;

TEST(form_data, basic) {
  std::string boundary = "TEST_BOUNDARY";
  std::string test_content = "--" + boundary + http::nl;

  test_content += "Content-Disposition: form-data; name=\"name\"\r\n\r\n";
  test_content += "TEST_NAME\r\n";
  test_content += "--" + boundary + http::nl;

  test_content += "Content-Disposition: form-data; name=\"message\"\r\n\r\n";
  test_content += "TEST_MESSAGE\r\n";
  test_content += "--" + boundary + "--" + http::nl;

  std::map<std::string, std::string> expected_vars = {
      {"name", "TEST_NAME"}, {"message", "TEST_MESSAGE"}};

  auto vars = http::parseFormdata(test_content, boundary);

  EXPECT_EQ(vars, expected_vars);
}
