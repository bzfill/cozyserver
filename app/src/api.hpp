#ifndef API_HPP
#define API_HPP

#include "application.hpp"
#include "applist.hpp"
#include "http.hpp"
#include "json.hpp"

namespace server {

class API : public Application {
public:
  virtual std::string name() const { return "API"; }
  virtual std::string pathPrefix() const { return "api"; }
  virtual std::string description() const {
    return "run applications' API methods";
  }

  virtual CreateAppFuncType sharedPtrLambda() {
    return []() -> std::shared_ptr<Application> {
      return std::make_shared<API>();
    };
  }

  virtual TaskResult run(http::Request req);

  virtual TaskResult next() {
    throw ApplicationError(*this, "next is not implemented");
  }

  static void setAppTable(AppTableType &app_table) { app_map = &app_table; }

private:
  static AppTableType *app_map;
};

} // namespace server

#endif
