#include "api.hpp"
#include "applist.hpp"
#include "config.hpp"
#include "http.hpp"
#include "server.hpp"
#include "settings.hpp"
#include "storeapp.hpp"
#include "wall.hpp"
#include <any>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>
#include <cstddef>
#include <filesystem>
#include <iostream>
#include <istream>
#include <memory>
#include <vector>

using namespace server;
namespace po = boost::program_options;

AppTableType application_list = {makeAppTableElement<Storage>(),
                                 makeAppTableElement<WallApp>(),
                                 makeAppTableElement<API>()};

void initAppTable() {
  TcpConnection::setAppTable(application_list);
  API::setAppTable(application_list);
}

void addStaticArgs(po::options_description &desc) {
  desc.add_options()("help,h", "Produce help.")(
      "gen-config,G", "Generate default config file and exit."
                      "file will be overwirtted.")(
      "default-config", "Don't load config file and use default values.")(
      "force", "Don't ask questions");
}

void addAppArgs(
    po::options_description &desc,
    server::configuration::ConfigManager &server_config,
    std::vector<std::tuple<std::string, std::string, std::any>> app_args) {

  auto apps = server_config.appList();

  for (auto &app : apps) {
    auto properties = server_config.propertiesList(app);

    for (auto &p : properties) {

      std::any prop = server_config.get(app, p);

      auto notifier = [=, &app_args](std::any val) {
        app_args.push_back({app, p, val});
      };

      auto name = app + "." + p;

      if (prop.type().name() == typeid(int).name()) {
        desc.add_options()(name.c_str(), po::value<int>()->notifier(notifier),
                           "parameter of type <int>");
      } else if (prop.type().name() == typeid(double).name()) {
        desc.add_options()(name.c_str(),
                           po::value<double>()->notifier(notifier),
                           "parameter of type <double>");
      } else if (prop.type().name() == typeid(std::string).name()) {
        desc.add_options()(name.c_str(),
                           po::value<std::string>()->notifier(notifier),
                           "parameter of type <string>");
      } else if (prop.type().name() == typeid(bool).name()) {
        desc.add_options()(name.c_str(), po::value<bool>()->notifier(notifier),
                           "parameter of type <bool>");
      } else {
        desc.add_options()(name.c_str(), po::value<bool>(),
                           "parameter of type <unknown>");
      }
    }
  }
}

void processCommandLineArgs(
    int argc, char **argv,
    server::configuration::ConfigManager &server_config) {

  std::vector<std::tuple<std::string, std::string, std::any>> app_args;
  po::options_description desc;
  addStaticArgs(desc);
  addAppArgs(desc, server_config, app_args);
  po::variables_map vm;

  try {
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);
  }

  catch (po::error &err) {
    std::cout << err.what() << "\n";
    std::cout << "commands : \n " << desc << "\n ";
    exit(1);
  }
  if (vm.count("help")) {
    std::cout << desc << "\n";
    exit(0);
  }

  if (vm.count("gen-config")) {
    if (std::filesystem::exists("config.json") && (vm.count("force") == 0)) {
      std::cout << "Config already exists. Would you like to overwite it? "
                   "(\'yes\' to accept): ";

      std::string ans;
      std::cin >> ans;
      if (ans == "yes") {
        server_config.createFile();
      } else {
        exit(1);
      }

    } else {
      server_config.createFile();
    }

    exit(0);
  }

  if (vm.count("default-config") == 0) {
    server_config.loadFromFile();
  }

  for (auto &app_arg : app_args) {
    server_config.set(std::get<0>(app_arg), std::get<1>(app_arg),
                      std::get<2>(app_arg));
  }
}

int main(int argc, char **argv) {
  using std::literals::operator""s;
  initAppTable();

  auto &server_config = server::configuration::ConfigManager::getInstance();
  processCommandLineArgs(argc, argv, server_config);

  boost::asio::io_context context;
  server::TcpSocketWrapper wrapper(context);
  server::TcpAcceptor acceptor(wrapper);
  context.run();
  return 0;
}
