#ifndef FILELOADER_HPP
#define FILELOADER_HPP

#include "application.hpp"
#include "settings.hpp"
#include <exception>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>

namespace server {

class FileNotFoundError : public NotFoundBaseError {
public:
  FileNotFoundError(std::string file_name) {
    using std::literals::operator""s;
    msg = "file ["s + file_name + "] is not found"s;
  };
  virtual const char *what() const noexcept override { return msg.c_str(); }

private:
  std::string msg;
};

// read files by chunks
class FileReader {
public:
  FileReader(std::string path, size_t chunk_size = 1024 * 1024 * 4)
      : chunk_size(chunk_size) {
    try {
      file_size = file_size_left = std::filesystem::file_size(path);
      is.open(path, std::ios::in | std::ios::binary);
      is.exceptions(std::ifstream::failbit);
      if (is.bad() || file_size == 0) {
        throw FileNotFoundError(path);
      }
    } catch (const std::filesystem::filesystem_error &e) {
      throw FileNotFoundError(path);
    }
  }

  //FileReader(std::string path, size_t chunk_size) : FileReader(path) {
  //  this->chunk_size = chunk_size;
  //}

  std::string getChunk() {
    size_t load_size =
        file_size_left > chunk_size ? chunk_size : file_size_left;

    std::string ret;
    ret.resize(load_size);

    if (finish) {
      return ret;
    }
    is.read(reinterpret_cast<char *>(ret.data()), load_size);

    file_size_left -= load_size;

    if (!file_size_left) {
      finish = true;
    }

    return ret;
  }

  bool noMoreChunks() { return finish; }

  size_t getFileSize() { return file_size; }

private:
  size_t chunk_size;
  size_t file_size = 0;
  size_t file_size_left = 0;
  bool finish = false;
  std::ifstream is;
};

} // namespace server
#endif
