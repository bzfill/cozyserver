#ifndef STATIC_CONTENT_HPP
#define STATIC_CONTENT_HPP
#include "http.hpp"
#include <string>

namespace server::static_content {

const http::Response
    RESPONSE_404_NOT_FOUND(http::StatusCode::C404_Not_Found,
                           http::Connection::CLOSE,
                           std::string("<H1>ERROR 404: NOT FOUND</H1>"));

const http::Response
    RESPONSE_403_FORBIDDEN(http::StatusCode::C403_Forbidden,
                           http::Connection::CLOSE,
                           std::string("<H1>ERROR 403: Forbidden</H1>"));

const http::Response RESPONSE_500_INTERNAL_SERVER_ERROR(
    http::StatusCode::C500_Internal_Server_Error, http::Connection::CLOSE,
    std::string("<H1>ERROR 500: Internal server error</H1>"));

} // namespace server::static_content
#endif
