//#include "application.hpp"
//#include "applist.hpp"
#include "json.hpp"
#include "varextractor.hpp"
#include <any>
#include <exception>
#include <filesystem>
#include <fstream>
#include <istream>
#include <map>
#include <stdexcept>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <vector>

#ifndef CONFIG_HPP
#define CONFIG_HPP

using json = nlohmann::json;

namespace server::configuration {

class ConfigError : public std::exception {
public:
  ConfigError(std::string msg) : msg(msg) {}
  const char *what() const noexcept override { return msg.c_str(); }

private:
  std::string msg;
};

class ConfigManager {
public:
  ConfigManager(const ConfigManager &) = delete;
  ConfigManager &operator=(const ConfigManager &) = delete;
  ~ConfigManager() = default;

  static ConfigManager &getInstance() {
    static ConfigManager ret;
    return ret;
  }
  void loadFromFile(std::string path = "config.json");
  void createFile(std::string path = "config.json");

  std::any get(std::string domain, std::string name) {
    return properties.at(domain).at(name);
  }

  bool exists(std::string domain, std::string name) {
    if (properties.find(domain) != properties.end()) {
      auto &&p = properties[domain];
      if (p.find(name) != p.end()) {
        return true;
      }
    }
    return false;
  }

  void set(std::string domain, std::string name, std::any property) {
    if (exists(domain, name)) {
      if (properties[domain][name].type() != property.type()) {
        throw ConfigError("Set operation failed. Types don't match.");
      }
    }
    properties[domain][name] = property;
  }

  std::vector<std::string> appList() {
    std::vector<std::string> ret;

    for (auto &p : properties) {
      ret.push_back(p.first);
    }

    return ret;
  }

  std::vector<std::string> propertiesList(std::string domain) {
    std::vector<std::string> ret;

    for (auto &p : properties.at(domain)) {
      ret.push_back(p.first);
    }

    return ret;
  }

private:
  ConfigManager() = default;
  std::map<std::string, std::map<std::string, std::any>> properties;
  static ConfigManager *singleton;
};

template <class App, class T> class Property {
public:
  Property(const std::string &name, const T &value)
      : property_name(name), default_value(value),
        server_config(ConfigManager::getInstance()) {
    App app;
    app_name = app.name();

    set(value);
  }

  void set(const T &value) {
    server_config.set(app_name, property_name, value);
  }

  T get() {
    return std::any_cast<T>(server_config.get(app_name, property_name));
  }

  T getDefault() { return default_value; }
  void setDefault() { set(default_value); }

  operator T() { return get(); }

private:
  T default_value;
  std::string property_name;
  std::string app_name;
  ConfigManager &server_config;
};

} // namespace server::configuration

#endif
