#include "config.hpp"
namespace server::configuration {

void ConfigManager::loadFromFile(std::string path) {
  using std::literals::operator""s;

  if (std::filesystem::exists(path)) {
    std::ifstream is(path);
    json j;
    is.exceptions(std::ios::badbit | std::ios::failbit);
    try {
      is >> j;
    } catch (std::iostream::failure &e) {
      throw ConfigError("can't read file \""s + path + "\": "s + e.what());
    }

    for (auto &app_name : j.items()) {
      for (auto &property : app_name.value().items()) {
        auto &&p = property.value();
        std::any value;
        if (p.is_number_integer()) {
          value = p.get<int>();
        } else if (p.is_number_float()) {
          value = p.get<double>();
        } else if (p.is_string()) {
          value = p.get<std::string>();
        } else if (p.is_boolean()) {
          value = p.get<bool>();
        } else {
          throw ConfigError("type doesn't supported");
        }

        if (!exists(app_name.key(), property.key())) {
          throw ConfigError("application is not found");
        }

        set(app_name.key(), property.key(), value);
      }
    }

  } else {
    createFile(path);
  }
}

void ConfigManager::createFile(std::string path) {
  json j;

  auto type_hash = []<class T>() constexpr->size_t { return 1; };

  for (auto &d : properties) {
    for (auto &p : d.second) {

      if (p.second.type().hash_code() == typeid(int).hash_code()) {
        j[d.first][p.first] = std::any_cast<int>(p.second);
      } else if (p.second.type().hash_code() == typeid(double).hash_code()) {
        j[d.first][p.first] = std::any_cast<double>(p.second);
      } else if (p.second.type().hash_code() ==
                 typeid(std::string).hash_code()) {
        j[d.first][p.first] = std::any_cast<std::string>(p.second);
      } else if (p.second.type().hash_code() == typeid(bool).hash_code()) {
        j[d.first][p.first] = std::any_cast<bool>(p.second);
      } else {
        throw ConfigError("type doesn't supported");
      }
    }
  }

  std::ofstream os(path, std::ios::trunc);
  os << j.dump(4);
}

} // namespace server::configuration
