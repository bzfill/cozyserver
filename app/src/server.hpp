#ifndef SERVER_HPP
#define SERVER_HPP

#include "application.hpp"
#include "applist.hpp"
#include "http.hpp"
#include "static_content.hpp"
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <cstddef>
#include <filesystem>
#include <functional>
#include <iostream>
#include <istream>
#include <map>
#include <memory>
#include <stdexcept>

namespace server {

// Wrapper interface for boost::asio's socket functions. It's needed for unit
// tests.
class TcpSocketWrapperInterface {
public:
  TcpSocketWrapperInterface() {}
  virtual ~TcpSocketWrapperInterface(){};
  virtual boost::asio::io_context &getContext() = 0;
  virtual void async_read_until(
      boost::asio::ip::tcp::socket &socket, boost::asio::streambuf &buf,
      std::string delim,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) = 0;
  virtual void async_read_exactly(
      boost::asio::ip::tcp::socket &socket, boost::asio::streambuf &buf,
      std::size_t len,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) = 0;
  virtual void async_write(
      boost::asio::ip::tcp::socket &socket,
      const boost::asio::mutable_buffers_1 &buf,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) = 0;
};

// This wrapper is used for actual code
class TcpSocketWrapper : public TcpSocketWrapperInterface {
public:
  ~TcpSocketWrapper() {}
  TcpSocketWrapper() = delete;
  TcpSocketWrapper(boost::asio::io_context &context) : context(context) {}
  boost::asio::io_context &getContext() override final { return context; }
  void async_read_until(
      boost::asio::ip::tcp::socket &socket, boost::asio::streambuf &buf,
      std::string delim,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) override final {
    boost::asio::async_read_until(socket, buf, delim, lambda);
  }

  void async_read_exactly(
      boost::asio::ip::tcp::socket &socket, boost::asio::streambuf &buf,
      std::size_t len,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) override final {
    boost::asio::async_read(socket, buf, boost::asio::transfer_exactly(len),
                            lambda);
  }

  void async_write(
      boost::asio::ip::tcp::socket &socket,
      const boost::asio::mutable_buffers_1 &buf,
      std::function<void(const boost::system::error_code &, std::size_t)>
          lambda) override final {

    boost::asio::async_write(socket, buf, lambda);
  }

private:
  boost::asio::io_context &context;
};

// TcpConnection class uses this table by default to map application path prefix
// to a function, that returns a new object of an Application class. The server
// uses the first directory (path prefix) in path (from http request) for
// routing the request to corresponding Application. Every time the server
// receives http request, it creates an Application class instance, that accepts
// this request.
//
// For example: "GET /store/filename.zip HTTP/1.1"
// here "store" is a path prefix and request will be redirected to "Storage"
// application

class TcpConnection : public std::enable_shared_from_this<TcpConnection> {
public:
  // It's necessery to store a pointer in shared_ptr, because connection
  // algorithm uses shared_from_this()
  static auto create(TcpSocketWrapperInterface &wrapper) {
    return std::shared_ptr<TcpConnection>(new TcpConnection(wrapper));
  }

  auto &getSocket() { return socket; }
  // entry point for new connections
  void start() {
    wrapper.async_read_until(
        socket, buf, "\r\n\r\n",
        [self = shared_from_this()](const boost::system::error_code &ec,
                                    std::size_t len) {
          self->handleRead(ec, len);
        });
  }
  // change default application table. It's used by tests.
  static void setAppTable(AppTableType &table) { app_map = &table; }

private:
  TcpConnection(TcpSocketWrapperInterface &wrapper)
      : socket(wrapper.getContext()), wrapper(wrapper) {}
  void handleRead(const boost::system::error_code &ec, std::size_t len);
  void handleWrite(const boost::system::error_code &ec, std::size_t len,
                   std::shared_ptr<server::Application> app);
  void handleContent(const boost::system::error_code &ec, std::size_t len);
  void executeApplication();

  static AppTableType *app_map;
  TcpSocketWrapperInterface &wrapper;
  boost::asio::ip::tcp::socket socket;
  boost::asio::streambuf buf;
  Application::TaskResult result;
  http::Request req;
};

class TcpAcceptor {

public:
  TcpAcceptor(TcpSocketWrapperInterface &wrapper)
      : acceptor(wrapper.getContext(), boost::asio::ip::tcp::endpoint(
                                           boost::asio::ip::tcp::v4(), 80)),
        wrapper(wrapper) {
    accept();
  }

private:
  void accept() {
    auto connection = TcpConnection::create(wrapper);
    acceptor.async_accept(
        connection->getSocket(),
        [this, connection](const boost::system::error_code &ec) {
          connection->start();
          this->accept();
        });
  }

  TcpSocketWrapperInterface &wrapper;
  boost::asio::ip::tcp::acceptor acceptor;
};

} // namespace server
#endif
