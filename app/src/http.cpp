#include "http.hpp"
#include "static_content.hpp"
#include <chrono>
#include <string>

namespace server::http {

void Request::parse(const std::string &req) {
  clear();
  
  std::vector<std::string> request_list;

  boost::split_regex(request_list, req, boost::regex("\r\n"));
  auto request_iterator = begin(request_list);


  parseRequestLine(*request_iterator++);

  std::vector<std::string> header;
  for (; request_iterator != end(request_list); request_iterator++) {
    if (*request_iterator != "") {
      boost::split_regex(header, *request_iterator, boost::regex(": "));
      if (header.size() == 2) {
        this->headers[header[0]] = header[1];
      }
    }
  }

  if (headers.find("Content-Type") != headers.end()) {

    auto &ct = headers["Content-Type"];

    auto pos = ct.find(";");
    try {
      this->content_type = content_type_map[ct.substr(0, pos)];

      const std::string tag = "boundary=";
      auto tag_pos = ct.find(tag);

      if (tag_pos != std::string::npos) {
        this->boundary = ct.substr(tag_pos + tag.length(), std::string::npos);
      }
    } catch (...) {
    }
  }
  return;
}

std::string Request::getPathAt(size_t pos) {

  size_t str_pos = 0;
  for (;
       pos > 0 && (str_pos = path.find("/", str_pos + 1)) != std::string::npos;
       pos--) {
  }
  auto ret =
      path.substr(str_pos + 1, path.find("/", str_pos + 1) - (str_pos + 1));
  return ret;
}

bool Request::operator==(const Request &rhs) const {

  if (this->method != rhs.method) {
    return false;
  }
  if (this->path != rhs.path) {
    return false;
  }
  if (this->version != rhs.version) {
    return false;
  }
  if (this->headers != rhs.headers) {
    return false;
  }

  return true;
}

void Request::parseRequestLine(const std::string &rl) {

  std::vector<std::string> rl_list;
  boost::split(rl_list, rl, boost::is_any_of(" "));

  if (rl_list.size() != 3)
    throw ParseError(rl);

  try {
    this->method = method_map[rl_list[0]];
  } catch (std::exception &e) {
    throw ParseError(rl);
  }

  auto pos = rl_list[1].find("?");

  this->path = rl_list[1].substr(0, pos);
  if (pos != std::string::npos) {
    auto var_str = rl_list[1].substr(pos + 1, std::string::npos);
    parseVariables(var_str);
  }

  try {
    this->version = version_map[rl_list[2]];
  } catch (std::exception &e) {
    throw ParseError(rl);
  }

  return;
}

void Request::parseVariables(const std::string &str_var) {
  std::vector<std::string> var_list;
  boost::split(var_list, str_var, boost::is_any_of("&"));

  std::vector<std::string> var;
  for (auto &v : var_list) {
    boost::split(var, v, boost::is_any_of("="));
    if (var.size() == 2) {
      this->variables[var[0]] = var[1];
    }
  }
}

void Request::clear() {
  variables.clear();
  headers.clear();
  content.reset();
}

std::string Response::gen() const {
  constexpr char br[] = "\r\n";
  std::string ret;

  char buf[128];
  time_t now = time(0);
  tm tm = *gmtime(&now);
  strftime(buf, sizeof(buf), SERVER_TIME_FORMAT, &tm);

  // Response line
  ret += HTTP_VERSION;
  ret += " ";
  ret += status_code_map[status_code.value()];
  ret += br;

  // headers
  ret += "Access-Control-Allow_origin: *";
  ret += br;

  if (connection) {
    ret += "Connection: ";
    ret += connection_map[connection.value()];
    ret += br;
  }

  ret += "Date: ";
  ret += buf;
  ret += br;

  ret += "Server: ";
  ret += SERVER_NAME;
  ret += br;

  if (location) {
    ret += "Location: ";
    ret += location.value();
    ret += br;
  }

  if (transfer_encoding) {
    ret += "Transfer-Encoding: ";
    ret += transfer_encoding_map[transfer_encoding.value()];
    ret += br;
  }

  ret += "X-Frame-Options: DENY";
  ret += br;

  if (content) {

    if (content_type) {
      ret += "Content-Type: ";
      ret += content_type_map[content_type.value()];
      ret += br;
    } else {
      throw GenError("content_type is an obligatory field");
    }

    if (content_encoding) {
      ret += "Content-Encoding: ";
      ret += content_encoding_map[content_encoding.value()];
      ret += br;
    }

    ret += "Content-Length: ";
    if (content_length) {
      ret += std::to_string(content_length.value());
    } else {
      ret += std::to_string(content.value().size());
    }
    ret += br;

    ret += br;

    ret += content.value();

  } else {
    ret += br;
  }

  return ret;
}

std::map<std::string, std::string> parseFormdata(std::string &data,
                                                 std::string &boundary) {
  using std::literals::operator""s;
  std::map<std::string, std::string> ret;
  std::vector<std::string> form_variables;
  boost::split_regex(form_variables, data, boost::regex("--"s + boundary));

  for (auto &v : form_variables) {
    if (v.empty() || v == ("--"s + nl)) {
      continue;
    }
    const auto prefix = "Content-Disposition: form-data; name=\""s;
    auto begin_pos = v.find(prefix);
    auto var_end_pos = v.find("\"", begin_pos + prefix.length());
    auto var_body_pos = v.find(br, var_end_pos);
    auto var_body_pos_end = v.find(nl, var_body_pos + br.length());

    if (begin_pos == std::string::npos || var_end_pos == std::string::npos ||
        var_body_pos == std::string::npos ||
        var_body_pos_end == std::string::npos) {

      throw ParseError(data);
    }
    auto &&var_name = v.substr(begin_pos + prefix.length(),
                               var_end_pos - prefix.length() - begin_pos);

    ret[var_name] = v.substr(var_body_pos + br.length(),
                             var_body_pos_end - var_body_pos - br.length());
  }

  return ret;
}
} // namespace server::http
