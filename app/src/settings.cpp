#include "settings.hpp"
namespace server {
configuration::Property<ServerConfig, std::string> ServerConfig::hostname{
    "hostname", "127.0.0.1"};
configuration::Property<ServerConfig, std::string> ServerConfig::server_name{
    "server_name", "Mocha"};
} // namespace server
