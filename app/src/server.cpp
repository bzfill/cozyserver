#include "server.hpp"
#include "application.hpp"
#include "wall.hpp"
#include <string>

using namespace server;

namespace server {

AppTableType *TcpConnection::app_map = nullptr;

void TcpConnection::handleRead(const boost::system::error_code &ec,
                               std::size_t len) {
  using std::operator""s;

  if (ec) {
    return;
  }

  std::string buf_str(boost::asio::buffers_begin(buf.data()),
                      boost::asio::buffers_begin(buf.data()) + len);

  buf.consume(len);
  this->req.parse(buf_str);

  auto content_length =
      this->req.headers.find("Content-Length") == this->req.headers.end()
          ? 0
          : std::stoi(this->req.headers["Content-Length"]);

  if (content_length == 0) {

    executeApplication();
  } else {
    this->wrapper.async_read_exactly(
        socket, buf, content_length,
        [self = this->shared_from_this()](const boost::system::error_code &ec,
                                          std::size_t len) {
          self->handleContent(ec, len);
        });
  }
}

void TcpConnection::handleWrite(const boost::system::error_code &ec,
                                std::size_t len,
                                std::shared_ptr<Application> app) {

  if (ec) {
    return;
  }

  auto result = app->next();

  if (result.finish) {
    wrapper.async_write(
        socket, boost::asio::buffer(result.data),
        [](const boost::system::error_code &ec, std::size_t len) {});

  } else {
    wrapper.async_write(
        socket, boost::asio::buffer(result.data),
        [self = shared_from_this(), app](const ::boost::system::error_code &ec,
                                         std::size_t len) {
          self->handleWrite(ec, len, app);
        });
  }
}

void TcpConnection::handleContent(const boost::system::error_code &ec,
                                  std::size_t len) {
  std::string buf_str(boost::asio::buffers_begin(buf.data()),
                      boost::asio::buffers_begin(buf.data()) + len);

  buf.consume(len);
  this->req.content = buf_str;
  executeApplication();
}

void TcpConnection::executeApplication() {
  try {

    auto &&app_str = req.getPathAt(0);

    // make Apllication class instance
    auto app = app_map->at(app_str)();
    req.cutBeginingOfPath();

    result = app->run(req);

    if (result.data == "") {
      throw ApplicationError(*app.get(), "app didn't return data");
    }

    if (result.finish) {
      wrapper.async_write(
          socket, boost::asio::buffer(result.data),
          [](const boost::system::error_code &ec, std::size_t len) {});
    } else {

      wrapper.async_write(
          socket, boost::asio::buffer(result.data),
          [self = shared_from_this(),
           app](const ::boost::system::error_code &ec, std::size_t len) {
            self->handleWrite(ec, len, app);
          });
    }

  } catch (std::out_of_range &e) {

    auto &&content = static_content::RESPONSE_404_NOT_FOUND.gen();

    wrapper.async_write(
        socket, boost::asio::buffer(content),
        [](const boost::system::error_code &ec, std::size_t len) {});

  } catch (ApplicationError &e) {

    auto &&content = static_content::RESPONSE_500_INTERNAL_SERVER_ERROR.gen();
    wrapper.async_write(
        socket, boost::asio::buffer(content),
        [](const boost::system::error_code &ec, std::size_t len) {});
  } catch (...) {
    auto &&content = static_content::RESPONSE_500_INTERNAL_SERVER_ERROR.gen();

    wrapper.async_write(
        socket, boost::asio::buffer(content),
        [](const boost::system::error_code &ec, std::size_t len) {});
  }

  start();
}

} // namespace server
