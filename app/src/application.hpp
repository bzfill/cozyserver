#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "http.hpp"
#include "json.hpp"
#include "settings.hpp"
#include "static_content.hpp"
#include <cstdint>
#include <exception>
#include <filesystem>
#include <fstream>
#include <functional>
#include <ios>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

using json = nlohmann::json;

namespace server {

// base class for all applications.
class Application {

public:
  struct TaskResult {
    bool finish;
    std::string data;
  };

  using CreateAppFuncType = std::function<std::shared_ptr<Application>()>;

  virtual ~Application() = default;
  virtual std::string name() const = 0;
  virtual std::string pathPrefix() const = 0;
  virtual std::string description() const = 0;
  // return a function which creates an object of this class
  virtual CreateAppFuncType sharedPtrLambda() = 0;
  virtual void defaultConfig() {}
  // entry point. Return the whole data or the first part of data with http
  // response headers
  virtual TaskResult run(http::Request) = 0;
  // if response must be splitted, this function is used to get next portion of
  // data
  virtual TaskResult next() = 0;
};

class ApplicationError : virtual public std::exception {
public:
  ApplicationError(const Application &app, std::string msg) : msg(msg) {
    using std::literals::operator""s;
    msg = "["s + this->errorName() + "::"s + app.name() + "]"s + " - "s + msg;
  }
  virtual const char *what() const noexcept override { return msg.c_str(); }
  virtual const char *errorName() const noexcept { return "ApplicationError"; }

private:
  std::string msg;
};

class NotFoundBaseError : virtual public std::exception {};

class AppLogicNotFoundError : public NotFoundBaseError,
                              public ApplicationError {
public:
  AppLogicNotFoundError(Application &app, std::string msg)
      : ApplicationError(app, msg) {}
  virtual const char *errorName() const noexcept override {
    return "AppLogicNotfounderror";
  };
};

class APIInterface {
public:
  virtual json
  executeMethod(const std::string &method,
                const std::map<std::string, std::string> &variables) const = 0;

  static constexpr char RUN_KEY[] = "__SERVER_RUN_APP";

protected:
  // This class extracts values from map<string, string> and converts it to some
  // type. This is usefull for executeMethod implemetation
};

} // namespace server
#endif
