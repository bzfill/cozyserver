#include "api.hpp"
#include "application.hpp"
#include "applist.hpp"
#include "http.hpp"
#include "settings.hpp"
#include <memory>
#include <stdexcept>
#include <vector>

namespace server {

AppTableType *API::app_map = nullptr;

Application::TaskResult API::run(http::Request req) {
  using std::literals::operator""s;

  Application::TaskResult ret;
  ret.finish = true;
  auto app_name = req.getPathAt(0);

  if (app_name == "") {
    throw ApplicationError(*this, "wrong path");
  }

  json result;

  std::shared_ptr<Application> app;
  try {

    // make Application class object
    app = app_map->at(app_name)();

    auto app_api = dynamic_cast<APIInterface *>(app.get());
    if (app_api == nullptr) { // doesn't have API
      throw ApplicationError(*this, "application doesn't have API");
    }

    auto api_method = req.getPathAt(1);
    if (api_method == "") {
      throw ApplicationError(*this, "empty API method");
    }

    if (req.method != http::Method::GET) {
      throw ApplicationError(*this, "only GET method is supported");
    }

    std::map<std::string, std::string> variables = req.variables;
    result = app_api->executeMethod(api_method, variables);
  } catch (std::out_of_range &e) {
    throw ApplicationError(*this, "application is not found");
  } catch (http::ParseError &e) {
    throw ApplicationError(*this, e.what());
  }
  http::Response resp;

  resp.status_code = http::StatusCode::C200_OK;
  resp.content_encoding = http::ContentEncoding::identity;
  resp.content_type = http::ContentType::application_json;
  resp.connection = http::Connection::CLOSE;
  resp.content = result.dump();

  ret.data = resp.gen();
  return ret;
}

} // namespace server
