#include "application.hpp"
#include "config.hpp"
#include "fileloader.hpp"
#include "http.hpp"
#include "settings.hpp"
#include "static_content.hpp"
#include <memory>
#include <string>
#include <vector>

#ifndef STOREAPP_HPP
#define STOREAPP_HPP

namespace server {

class Storage : public Application {
public:
  ~Storage() = default;

  virtual std::string name() const override { return ("Storage"); }
  virtual std::string pathPrefix() const override { return ("store"); }
  virtual std::string description() const override { return ("Loads files"); }
  virtual CreateAppFuncType sharedPtrLambda() override {
    return []() -> std::shared_ptr<Application> {
      return std::make_shared<Storage>();
    };
  }
  virtual void defaultConfig() override {
    config.path.setDefault();
    config.chunk_size.setDefault();
  }

  TaskResult run(http::Request req) override;
  TaskResult next() override;

protected:
  TaskResult loadFile(const std::string &path);
  void setRoot(std::string path) { path_root = path; }

private:
  static struct Config {
    configuration::Property<Storage, std::string> path{"path", "."};
    configuration::Property<Storage, int> chunk_size{"chunk_size",
                                                     1024 * 1024 * 4}; // 4 MiB
  } config;
  http::ContentType fileFormat(std::string path);
  std::string path_root;
  std::ifstream file;
  std::unique_ptr<FileReader> reader;
};

} // namespace server

#endif
