#ifndef HTTP_H
#define HTTP_H

#include "server_definitions.hpp"
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string_regex.hpp>
#include <chrono>
#include <cstdint>
#include <ctime>
#include <exception>
#include <initializer_list>
#include <iostream>
#include <map>
#include <optional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace server::http {

///////////////////////////////////////////
// Enums for http requests and responces //
///////////////////////////////////////////

enum class Method { GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, TRACE };

enum class Version { HTTP_0_9, HTTP_1_0, HTTP_1_1, HTTP_2_0 };

enum class Connection { KEEP_ALIVE, CLOSE };

enum class ContentType {
  text_html,
  text_js,
  text_css,
  image_jpeg,
  image_png,
  font_ttf,
  font_otf,
  font_woff,
  font_woff2,
  application_octet_stream,
  application_json,
  application_x_www_form_urlencoded,
  multipart_form_data
};

enum class ContentEncoding { gzip, compress, deflate, br, identity };

enum class TransferEncoding { chunked, compress, deflate, gzip, identity };

enum class StatusCode {
  C100_Continue,
  C101_Switching_Protocols,
  C200_OK,
  C201_Created,
  C202_Accepted,
  C203_Non_Authoritative_Information,
  C204_No_Content,
  C205_Reset_Content,
  C206_Partial_Content,
  C300_Multiple_Choices,
  C301_Moved_Permanently,
  C302_Found,
  C303_See_Other,
  C304_Not_Modified,
  C305_Use_Proxy,
  C307_Temporary_Redirect,
  C400_Bad_Request,
  C401_Unauthorized,
  C402_Payment_Required,
  C403_Forbidden,
  C404_Not_Found,
  C405_Method_Not_Allowed,
  C406_Not_Acceptable,
  C407_Proxy_Authentication_Required,
  C408_Request_Timeout,
  C409_Conflict,
  C410_Gone,
  C411_Length_Required,
  C412_Precondition_Failed,
  C413_Request_Entity_Too_Large,
  C414_Request_URI_Too_Long,
  C415_Unsupported_Media_Type,
  C416_Requested_Range_Not_Satisfiable,
  C417_Expectation_Failed,
  C500_Internal_Server_Error,
  C501_Not_Implemented,
  C502_Bad_Gateway,
  C503_Service_Unavailable,
  C504_Gateway_Timeout,
  C505_HTTP_Version_Not_Supported
};

/////////////////////
// Enum converters //
/////////////////////

template <class L, class R> class EnumConverter {
public:
  EnumConverter(std::initializer_list<std::pair<L, R>> list) {
    for (auto &elem : list) {
      direct[elem.first] = elem.second;
      reverse[elem.second] = elem.first;
    }
  }

  R operator[](const L &key) const { return direct.at(key); }

  L operator[](const R &key) const { return reverse.at(key); }

private:
  std::unordered_map<L, R> direct;
  std::unordered_map<R, L> reverse;
};

const EnumConverter<std::string, Method> method_map = {
    {"GET", Method::GET},         {"HEAD", Method::HEAD},
    {"POST", Method::POST},       {"PUT", Method::PUT},
    {"DELETE", Method::DELETE},   {"CONNECT", Method::CONNECT},
    {"OPTIONS", Method::OPTIONS}, {"TRACE", Method::TRACE}};

const EnumConverter<std::string, Version> version_map = {
    {"HTTP/0.9", Version::HTTP_0_9},
    {"HTTP/1.0", Version::HTTP_1_0},
    {"HTTP/1.1", Version::HTTP_1_1},
    {"HTTP/2.0", Version::HTTP_2_0},
};

const EnumConverter<std::string, Connection> connection_map = {
    {"keep-alive", Connection::KEEP_ALIVE}, {"close", Connection::CLOSE}};

const EnumConverter<std::string, ContentType> content_type_map = {
    {"text/html", ContentType::text_html},
    {"text/js", ContentType::text_js},
    {"text/css", ContentType::text_css},
    {"image/jpeg", ContentType::image_jpeg},
    {"image/png", ContentType::image_png},
    {"font/woff", ContentType::font_woff},
    {"font/woff2", ContentType::font_woff2},
    {"font/ttf", ContentType::font_ttf},
    {"font/otf", ContentType::font_otf},
    {"application/octet-stream", ContentType::application_octet_stream},
    {"application/json", ContentType::application_json},
    {"application/x-www-form-urlencoded",
     ContentType::application_x_www_form_urlencoded},
    {"multipart/form-data", ContentType::multipart_form_data}};

const EnumConverter<std::string, ContentEncoding> content_encoding_map = {
    {"gzip", ContentEncoding::gzip},
    {"compress", ContentEncoding::compress},
    {"deflate", ContentEncoding::deflate},
    {"br", ContentEncoding::br},
    {"identity", ContentEncoding::identity}};

const EnumConverter<StatusCode, std::string> status_code_map = {
    {StatusCode::C100_Continue, "100 Continue"},
    {StatusCode::C101_Switching_Protocols, "101 Switching Protocols"},
    {StatusCode::C200_OK, "200 OK"},
    {StatusCode::C201_Created, "201 Created"},
    {StatusCode::C202_Accepted, "202 Accepted"},
    {StatusCode::C203_Non_Authoritative_Information,
     "203 Non Authoritative Information"},
    {StatusCode::C204_No_Content, "204 No Content"},
    {StatusCode::C205_Reset_Content, "205 Reset Content"},
    {StatusCode::C206_Partial_Content, "206 Partial Content"},
    {StatusCode::C300_Multiple_Choices, "300 Multiple Choices"},
    {StatusCode::C301_Moved_Permanently, "301 Moved Permanently"},
    {StatusCode::C302_Found, "302 Found"},
    {StatusCode::C303_See_Other, "303 See Other"},
    {StatusCode::C304_Not_Modified, "304 Not Modified"},
    {StatusCode::C305_Use_Proxy, "305 Use Proxy"},
    {StatusCode::C307_Temporary_Redirect, "307 Temporary Redirect"},
    {StatusCode::C400_Bad_Request, "400 Bad Request"},
    {StatusCode::C401_Unauthorized, "401 Unauthorized"},
    {StatusCode::C402_Payment_Required, "402 Payment Required"},
    {StatusCode::C403_Forbidden, "403 Forbidden"},
    {StatusCode::C404_Not_Found, "404 Not Found"},
    {StatusCode::C405_Method_Not_Allowed, "405 Method Not Allowed"},
    {StatusCode::C406_Not_Acceptable, "406 Not Acceptable"},
    {StatusCode::C407_Proxy_Authentication_Required,
     "407 Proxy Authentication Required"},
    {StatusCode::C408_Request_Timeout, "408 Request Timeout"},
    {StatusCode::C409_Conflict, "409 Conflict"},
    {StatusCode::C410_Gone, "410 Gone"},
    {StatusCode::C411_Length_Required, "411 Length Required"},
    {StatusCode::C412_Precondition_Failed, "412 Precondition Failed"},
    {StatusCode::C413_Request_Entity_Too_Large, "413 Request Entity Too Large"},
    {StatusCode::C414_Request_URI_Too_Long, "414 Request URI Too Long"},
    {StatusCode::C415_Unsupported_Media_Type, "415 Unsupported Media Type"},
    {StatusCode::C416_Requested_Range_Not_Satisfiable,
     "416 Requested Range Not Satisfiable"},
    {StatusCode::C417_Expectation_Failed, "417 Expectation Failed"},
    {StatusCode::C500_Internal_Server_Error, "500 Internal Server Error"},
    {StatusCode::C501_Not_Implemented, "501 Not Implemented"},
    {StatusCode::C502_Bad_Gateway, "502 Bad Gateway"},
    {StatusCode::C503_Service_Unavailable, "503 Service Unavailable"},
    {StatusCode::C504_Gateway_Timeout, "504 Gateway Timeout"},
    {StatusCode::C505_HTTP_Version_Not_Supported,
     "505 HTTP Version Not Supported"},
};

const EnumConverter<TransferEncoding, std::string> transfer_encoding_map = {
    {TransferEncoding::gzip, "gzip"},
    {TransferEncoding::compress, "compress"},
    {TransferEncoding::deflate, "deflate"},
    {TransferEncoding::chunked, "chunked"},
    {TransferEncoding::identity, "identity"}};

///////////////
// constants //
///////////////

const std::string nl = "\r\n";
const std::string br = "\r\n\r\n";

/////////////
// classes //
/////////////

class ParseError : public std::exception {
public:
  ParseError(const std::string &expression) {
    msg = "failed to parse expression: <" + expression + ">";
  }

  const char *what() const noexcept override { return msg.c_str(); }

private:
  std::string msg;
};

class GenError : public std::exception {
public:
  GenError(const std::string &msg) {
    this->msg = "failed to generate response: <" + msg + ">";
  }

  const char *what() const noexcept override { return msg.c_str(); }

private:
  std::string msg;
};

struct Request {
  Request() = default;
  Request(const std::string &req) { parse(req); }
  void parse(const std::string &req);

  Method method;
  std::string path;
  Version version;
  std::map<std::string, std::string> variables;
  std::map<std::string, std::string> headers;
  std::optional<std::string> content;
  std::optional<ContentType> content_type;
  std::string boundary;

  std::string getPathAt(size_t pos);

  void cutBeginingOfPath() {
    auto pos = path.find("/", 1);

    if (pos == std::string::npos) {
      path = "/";
      return;
    }
    auto &&cutted = path.substr(pos, std::string::npos);
    if (cutted == "") {
      cutted = "/";
    }
    path = cutted;
  }

  bool operator==(const Request &rhs) const;

private:
  void clear();
  void parseRequestLine(const std::string &rl);
  void parseVariables(const std::string &str_var);
};

struct Response {
  Response() = default;
  Response(StatusCode status_code, Connection connection, std::string cnt)
      : status_code(status_code), connection(connection), content(cnt) {
    content_type = ContentType::text_html;
    content_encoding = ContentEncoding::identity;
  }

  std::optional<StatusCode> status_code;
  std::optional<TransferEncoding> transfer_encoding;
  std::optional<ContentEncoding> content_encoding;
  std::optional<Connection> connection;
  std::optional<ContentType> content_type;
  std::optional<std::string> content;
  std::optional<size_t> content_length;
  std::optional<std::string> location;

  std::string gen() const;

private:
  static constexpr char HTTP_VERSION[] = "HTTP/1.1";

  const uint8_t *data;
};

///////////////
// functions //
///////////////

std::map<std::string, std::string> parseFormdata(std::string &data,
                                                 std::string &boundary);

} // namespace server::http

#endif
