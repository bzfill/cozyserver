#include "wall.hpp"
#include "application.hpp"
#include "http.hpp"
#include "json.hpp"
#include "settings.hpp"
#include <boost/algorithm/string.hpp>
#include <functional>
#include <memory>
#include <pqxx/connection.hxx>
#include <pqxx/except.hxx>
#include <pqxx/result.hxx>
#include <pqxx/transaction.hxx>
#include <sstream>
#include <stdexcept>
#include <string>
#include <string_view>
#include <type_traits>
#include <vector>

namespace server::db {

DBWorker::DBWorker(std::string db_name, std::string table_name)
    : db_name(db_name), table_name(table_name) {

  using std::literals::operator""s;

  if (!DBExists()) {
    createDB();
  }
  db_connect = std::make_unique<pqxx::connection>("dbname="s + db_name);

  if (!tableExists()) {
    createTable();
  }
}

MessagesType DBWorker::getMessages(size_t offset, size_t limit) {
  MessagesType ret;

  using std::literals::operator""s;

  try {
    pqxx::work w(*this->db_connect);
    pqxx::result res(w.exec("SELECT * FROM "s + this->table_name + " LIMIT " +
                            std::to_string(limit) + " OFFSET " +
                            std::to_string(offset)));
    w.commit();

    for (const auto &row : res) {
      std::vector<std::string_view> v;
      MessageType m;
      m.id = row.at(0).view();
      m.name = row.at(1).view();
      m.message = row.at(2).view();
      m.date = row.at(3).view();

      ret.push_back(m);
    }

  } catch (pqxx::sql_error &e) {
    throw DBError("sql error: "s + e.what());
  } catch (pqxx::broken_connection) {
    throw DBError("connection lost");
  } catch (...) {
    throw DBError("undefined error");
  }

  return ret;
}

void DBWorker::addMessage(std::string name, std::string message) {
  using std::literals::operator""s;

  try {
    pqxx::work w(*this->db_connect);
    w.exec0("INSERT INTO "s + this->table_name + " VALUES(DEFAULT, \'" + name +
            "\', \'" + message + "\', DEFAULT)");
    w.commit();
  } catch (pqxx::sql_error &e) {
    throw DBError("sql error: "s + e.what());
  } catch (pqxx::broken_connection) {
    throw DBError("connection lost");
  } catch (...) {
    throw DBError("undefined error");
  }
}

void DBWorker::deleteMessage(std::string id) {
  using std::literals::operator""s;

  try {
    pqxx::work w(*this->db_connect);
    w.exec0("DELETE FROM "s + this->table_name + " WHERE id = "s + id);
    w.commit();
  } catch (pqxx::sql_error &e) {
    throw DBError("sql error: "s + e.what());
  } catch (pqxx::broken_connection) {
    throw DBError("connection lost");
  } catch (...) {
    throw DBError("undefined error");
  }
}

bool DBWorker::DBExists() {
  try {
    using std::literals::operator""s;
    pqxx::connection conn("dbname=postgres");
    pqxx::work w(conn);
    auto res =
        w.exec1("SELECT EXISTS (SELECT datname FROM pg_catalog.pg_database "
                "WHERE datname = \'"s +
                db_name + "\')"s);
    w.commit();

    if (res.at(0).view() == "t") {
      return true;
    } else {
      return false;
    }

  } catch (pqxx::broken_connection &e) {
    throw DBError("can't connect to data base");
  } catch (...) {
    throw DBError("undefined error");
  }
}
bool DBWorker::tableExists() {
  using std::literals::operator""s;
  try {
    pqxx::work w(*this->db_connect);
    auto res = w.exec1(
        "SELECT EXISTS (SELECT table_name FROM information_schema.tables "
        "WHERE table_name = \'"s +
        table_name + "\')"s);
    w.commit();

    if (res.at(0).view() == "t") {
      return true;
    } else {
      return false;
    }

  } catch (pqxx::broken_connection &e) {
    throw DBError("can't connect to data base");
  } catch (...) {
    throw DBError("undefined error");
  }
}

void DBWorker::createDB() {
  using std::literals::operator""s;

  try {
    pqxx::connection conn("dbname=postgres");
    pqxx::nontransaction w(conn);
    w.exec0("CREATE DATABASE "s + this->db_name);

  } catch (pqxx::sql_error &e) {
    throw DBError("sql error: "s + e.what());
  } catch (pqxx::broken_connection) {
    throw DBError("connection lost");
  } catch (...) {
    throw DBError("undefined error");
  }
}

void DBWorker::createTable() {
  using std::literals::operator""s;

  try {
    pqxx::nontransaction w(*this->db_connect);
    w.exec0("CREATE TABLE "s + this->table_name +
            " (id SERIAL PRIMARY KEY, name varchar(32), message varchar(5000), "
            "date timestamp DEFAULT CURRENT_TIMESTAMP)");

  } catch (pqxx::sql_error &e) {
    throw DBError("sql error: "s + e.what());
  } catch (pqxx::broken_connection) {
    throw DBError("connection lost");
  } catch (...) {
    throw DBError("undefined error");
  }
}

} // namespace server::db

namespace server {

WallApp::Config WallApp::config;

Application::TaskResult WallApp::run(http::Request req) {

  Storage::setRoot(config.path);
  const std::string index = "/index.html";

  if (req.method == http::Method::POST &&
      (req.content_type.has_value() &&
       req.content_type.value() == http::ContentType::multipart_form_data) &&
      req.content.has_value() && req.boundary != "") {

    auto variables = http::parseFormdata(req.content.value(), req.boundary);
    VariableExtractor ve(variables);

    try {
      auto name = ve.get<std::string>("name");
      auto message = ve.get<std::string>("message");
      this->db->addMessage(name, message);

      return Storage::loadFile(index);
    } catch (std::out_of_range &e) {

      return Storage::loadFile(index);
    } catch (db::DBError &e) {

      return Storage::loadFile(index);
    }
  } else {
    if (req.path == "/") {
      req.path = "/index.html";
    }
    return Storage::run(req);
  }
}

json WallApp::executeMethod(
    const std::string &method,
    const std::map<std::string, std::string> &variables) const {

  using std::literals::operator""s;

  VariableExtractor ve(variables);
  json ret;

  if (method == "get_messages") {

    size_t limit = ve.get("limit", int(config.default_limit));
    size_t offset = ve.get("offset", 0);

    auto msgs = this->db->getMessages(offset, limit);

    for (auto &m : msgs) {
      ret["messages"] += {{"id", m.id},
                          {"name", m.name},
                          {"message", m.message},
                          {"date", m.date}};
    }
  }
  if (method == "add_message") {
    try {
      auto name = ve.get<std::string>("name");
      auto message = ve.get<std::string>("message");

      this->db->addMessage(name, message);
    } catch (std::out_of_range &e) {
      ret["STATUS"] = "ERROR";
      ret["INFO"] = e.what();
      return ret;
    } catch (db::DBError &e) {
      ret["STATUS"] = "ERROR";
      ret["INFO"] = e.what();
      return ret;
    }

    ret["STATUS"] = "OK";
  }

  return ret;
}

} // namespace server
