#ifndef SERVER_DEFINITIONS_HPP
#define SERVER_DEFINITIONS_HPP

constexpr char SERVER_NAME[] = "Mocha";
constexpr char SERVER_TIME_FORMAT[] = "%a, %d %b %Y %H:%M:%S %Z";

#endif
