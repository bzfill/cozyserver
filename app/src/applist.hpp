#ifndef APPLIST_HPP
#define APPLIST_HPP

#include "application.hpp"

namespace server {

using AppTableType =
    std::map<std::string, server::Application::CreateAppFuncType>;

using AppTableElementType =
    std::pair<std::string, server::Application::CreateAppFuncType>;


// make an application table element for Application class
template <class A> AppTableElementType makeAppTableElement() {
  A app;
  return {app.pathPrefix(), app.sharedPtrLambda()};
}

} // namespace server

#endif
