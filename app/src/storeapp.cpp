#include "storeapp.hpp"

using namespace server;

Storage::Config Storage::config;

Application::TaskResult Storage::run(http::Request req) {
  if (path_root == "") {
    path_root = config.path;
  }

  TaskResult ret;
  try {

    if (req.path.find("..") != std::string::npos) {
      throw AppLogicNotFoundError(*this, "relative path is not allowed");
    }

    if (req.method == http::Method::GET) {
      ret = loadFile(req.path);
    }

    else {
      throw AppLogicNotFoundError(*this, "wrong method");
    }
  } catch (const NotFoundBaseError &e) {
    ret.finish = true;
    ret.data = static_content::RESPONSE_404_NOT_FOUND.gen();
  } catch (...) {
    throw ApplicationError(*this, "undefined error");
  }

  return ret;
}

Application::TaskResult Storage::loadFile(const std::string &path) {
  TaskResult ret;
  http::Response resp;
  std::string full_path = path_root + path;

  try {
    reader = std::make_unique<FileReader>(full_path, config.chunk_size);

    resp.content_type = fileFormat(path);
    resp.content_encoding = http::ContentEncoding::identity;
    resp.status_code = http::StatusCode::C200_OK;
    resp.content = reader->getChunk();
    resp.content_length = reader->getFileSize();
    resp.connection = http::Connection::CLOSE;

    ret.data = resp.gen();
    ret.finish = reader->noMoreChunks();
  } catch (const NotFoundBaseError &e) {
    ret.finish = true;
    ret.data = static_content::RESPONSE_404_NOT_FOUND.gen();
  }
  return ret;
}

Application::TaskResult Storage::next() {
  TaskResult ret;
  auto &&chunk = reader->getChunk();
  ret.data = std::string(begin(chunk), end(chunk));
  ret.finish = reader->noMoreChunks();
  return ret;
}

http::ContentType Storage::fileFormat(std::string path) {
  static const std::map<std::string, http::ContentType> media_type_map = {
      {".html", http::ContentType::text_html},
      {".htm", http::ContentType::text_html},
      {".js", http::ContentType::text_js},
      {".css", http::ContentType::text_css},
      {".jpg", http::ContentType::image_jpeg},
      {".jpeg", http::ContentType::image_jpeg},
      {".png", http::ContentType::image_png},
      {".woff", http::ContentType::font_woff},
      {".woff2", http::ContentType::font_woff2},
      {".ttf", http::ContentType::font_ttf},
      {".otf", http::ContentType::font_otf},
  };

  http::ContentType ret;
  auto dot_pos = path.rfind(".");
  if (dot_pos != std::string::npos) {
    std::string ff = path.substr(dot_pos, path.size());

    if (media_type_map.count(ff) > 0) {
      ret = media_type_map.at(ff);
    } else {
      ret = http::ContentType::application_octet_stream;
    }
  }
  return ret;
}
