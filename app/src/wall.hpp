#ifndef WALL_HPP
#define WALL_HPP

#include "application.hpp"
#include "config.hpp"
#include "fileloader.hpp"
#include "http.hpp"
#include "json.hpp"
#include "storeapp.hpp"
#include <exception>
#include <memory>
#include <pqxx/connection.hxx>
#include <pqxx/pqxx>
#include <regex>
#include <stdexcept>
#include <string>
#include <vector>

namespace server {

namespace db {

struct MessageType {
  std::string id;
  std::string name;
  std::string message;
  std::string date;
};

using MessagesType = std::vector<MessageType>;

class DBError : public std::exception {
public:
  DBError(std::string message) : msg(message) {}
  const char *what() const noexcept override { return msg.c_str(); }

private:
  std::string msg;
};

class DBWorkerInterface {
public:
  DBWorkerInterface(){};
  virtual ~DBWorkerInterface(){};

  virtual MessagesType getMessages(size_t offset = 0, size_t limit = 20) = 0;
  virtual void addMessage(std::string name, std::string message) = 0;
  virtual void deleteMessage(std::string id) = 0;
};

class DBWorker : public DBWorkerInterface {
public:
  DBWorker() = delete;
  DBWorker(std::string db_name, std::string table_name);
  virtual ~DBWorker(){};

  virtual MessagesType getMessages(size_t offset = 0,
                                   size_t limit = 20) override;
  virtual void addMessage(std::string name, std::string message) override;
  virtual void deleteMessage(std::string id) override;

private:
  bool DBExists();
  bool tableExists();
  void createDB();
  void createTable();
  std::string db_name;
  std::string table_name;
  std::unique_ptr<pqxx::connection> db_connect;
};

} // namespace db

class WallApp : public Storage, public APIInterface {
public:
  ~WallApp() = default;
  WallApp() = default;

  virtual std::string name() const override final { return "WallApp"; };
  virtual std::string pathPrefix() const override final { return "wall"; }
  virtual std::string description() const override final {
    return "message board";
  }
  virtual CreateAppFuncType sharedPtrLambda() override final {
    return []() -> std::shared_ptr<Application> {
      return std::make_shared<WallApp>();
    };
  }

  virtual json executeMethod(
      const std::string &method,
      const std::map<std::string, std::string> &variables) const override;

  virtual TaskResult run(http::Request req) override final;
  void setDBInterface(std::shared_ptr<db::DBWorkerInterface> interface) {
    this->db = interface;
  }

  virtual void defaultConfig() override final {
    config.default_limit.setDefault();
    config.path.setDefault();
  }

private:
  static struct Config {
    configuration::Property<WallApp, std::string> path{"path", "."};
    configuration::Property<WallApp, int> default_limit{"default_limit", 20};
  } config;

  std::shared_ptr<db::DBWorkerInterface> db =
      std::make_shared<db::DBWorker>("wallapp", "messages");
  std::string genMessages(std::string_view id, std::string_view name,
                          std::string_view date, std::string_view msg);
};

} // namespace server

#endif
