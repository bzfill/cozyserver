add_library(configlib STATIC config.cpp)
target_link_libraries(configlib
  PRIVATE project_config
  )

add_library(settings STATIC settings.cpp)
target_link_libraries(settings PRIVATE project_config)
#target_compile_features(settings PRIVATE cxx_std_20)

add_library(http STATIC http.cpp)
target_link_libraries(http PRIVATE project_config)

add_library(storeapp STATIC storeapp.cpp)
target_link_libraries(storeapp
  PRIVATE project_config
  PRIVATE configlib
  )

add_library(api STATIC api.cpp)
target_link_libraries(api PRIVATE project_config)

find_library(LIBPQXX NAMES pqxx REQUIRED)
find_library(LIBPQ NAMES pq REQUIRED)

add_library(wallapp STATIC wall.cpp)
target_link_libraries(wallapp
  PRIVATE configlib
  PRIVATE project_config
  PRIVATE ${LIBPQXX}
  PRIVATE ${LIBPQ}
  )


add_library(server STATIC server.cpp)
target_link_libraries(server
  PUBLIC project_config
  PUBLIC settings
  )

target_link_libraries(${PROJECT_NAME}
  PRIVATE http
  PRIVATE server
  PUBLIC storeapp
  PUBLIC wallapp
  PUBLIC api
  PUBLIC configlib
  )
