#include <map>
#include <string>

#ifndef VAREXTRACTOR_HPP
#define VAREXTRACTOR_HPP

namespace server {

class VariableExtractor {
public:
  VariableExtractor() = delete;
  VariableExtractor(const std::map<std::string, std::string> &variables)
      : variables(variables) {}

  template <class T> T get(std::string &&name, T def) {
    if (variables.find(name) == variables.end()) {
      return def;
    } else {
      return convertString<T>(variables.at(name));
    }
  }

  template <class T> T get(std::string &&name) {
    using std::literals::operator""s;

    if (variables.find(name) == variables.end()) {
      throw std::out_of_range("variable <"s + name + "> is not found");
    } else {
      return convertString<T>(variables.at(name));
    }
  }

private:
  template <class T> T convertString(const std::string &str) {
    constexpr int flag = 1;

    if constexpr (std::is_convertible_v<T, std::uintmax_t>) {
      return std::stoull(str);
    } else if constexpr (std::is_convertible_v<T, std::intmax_t>) {
      return std::stoll(str);
    } else if constexpr (std::is_same_v<T, std::string>) {
      return str;
    } else if constexpr (std::is_same_v<T, const char *>) {
      return str.c_str();
    } else {
      throw std::invalid_argument("conversion is not found");
    }
  }

  const std::map<std::string, std::string> &variables;
};

} // namespace server

#endif
