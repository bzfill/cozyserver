#ifndef SETTINGS_HPP
#define SETTINGS_HPP
#include "config.hpp"
#include <string>

namespace server {
struct ServerConfig {
  std::string name() { return "Server"; }
  static configuration::Property<ServerConfig, std::string> hostname;
  static configuration::Property<ServerConfig, std::string> server_name;
};

} // namespace server
#endif
